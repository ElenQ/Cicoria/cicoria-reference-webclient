var _ = require('underscore')
var qs = require('qs')
var route = require('page')

function render(template, data) {

  /**
   *   Renders the passed template in the main view
   */
  document.body.removeChild(VIEW)
  var tmpl = _.template(t[template])
  var html = tmpl(data)
  VIEW = template_to_dom(html)
  document.body.appendChild(VIEW)
  return inject_images(VIEW)
}

function init_view() {

  /**  TODO TODO => make the templates and t = require('./templates')
   *   Creates the main view.
   *   User defined template for the menu and a Div for the contents
   *
   */
  var tmpl
  tmpl = _.template(t['menu'])
  document.body.appendChild(template_to_dom(tmpl({})))
  VIEW = document.createElement('div')
  return document.body.appendChild(VIEW)
}

function init(context, next) {

  /**
   *   It runs before any callback.
   *
   *   If the user just arrived:
   *       - Downloads the database
   *       - Paints the main template
   *
   *   If the database is not reached:
   *       - Create a fake DB with one error post
   *       - Open that error post -> 500 internal server error
   *
   */
  if (DATA == null) {
    var iters = 5
    while (!((DATA != null) || iters === 0)) {
      --iters
      DATA = JSON.parse(getter.getSync({
        http: '/Metadata.json'
      }))
    }
    if (DATA == null) {
      DATA = {
        id: '500',
        title: 'Internal Server Error',
        short_desc: 'Impossible to download Database',
        category: 'error'
      }
      route('#!/500')
    }
    init_view()
  }
  return next()
}


function index(ctx, next) {
  /**
   *   Render the index page.
   *   Filtering is handled with the querystring
   *
   */
  console.log("INDEX")
  var filters = qs.parse(ctx.querystring)
  var d = DATA.posts
  d = _.filter(d, function(el) {
    return !el.hidden === true
  })
  if (filters.tags != null) {
    var filters_tags = [].concat(filters.tags)
    d = _.filter(d, function(el) {
      return _.some(el.tags, function(tag) {
        return indexOf.call(filters_tags, tag) >= 0
      })
    })
  }
  if (filters.category != null) {
    d = _.filter(d, function(el) {
      return el.category === filters.category
    })
  }
  if (filters.author != null) {
    d = _.filter(d, function(el) {
      return el.author === filters.author
    })
  }
  return render('index', {
    posts: d
  })
}

function post(){
  // TODO
  console.log('Post')
  return
}

function file_or_folder(){
  // TODO
  console.log('File or folder')
  return
}

window.onload = function() {
  route('*', init)
  route('/', index)
  route('/:post', post)
  route('/*', file_or_folder)
  route({ hashbang: true })
}
