# Cicoria Reference WebClient

A reference web based client for Cicoria sites. Consider it a PoC since the
project itself is a PoC.

More info [here](https://gitlab.com/ElenQ/Cicoria/cicoria-creator).
